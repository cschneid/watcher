module Main where

import BasePrelude

import Watcher.Monitor
import Watcher.Monitor.GraphiteReading
import Watcher.WebServer

main :: IO ()
main = do
  putStrLn "Starting Server In Background"
  mon   <- sequence [graphiteReadingMonitor (Configuration "")]

  void $ startMonitorThreads mon
  void $ startWebServerThread mon
  void $ forever $ do
    result <- atomically $ readTVar $ results (head mon)
    print $ "WTF" ++ show result
    threadDelay 100000

startMonitorThreads :: [Monitor] -> IO ()
startMonitorThreads = mapM_ forkMonitor

