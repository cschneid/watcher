module Network.Graphite.Client where

import BasePrelude

import qualified Network.Graphite.TimeSpecification as GTime

import Control.Lens         ( (.~), (^.), (&) )
import Data.Aeson           ( (.:), decode, Value(..), FromJSON(..) )
import Data.ByteString.Lazy ( ByteString )
import Data.Text            ( Text, pack )
import Network.Wreq         ( Options, Response, defaults, getWith, param, responseBody )

-- JSON Parse the data returned by Graphite
metricsJSON :: GraphiteRequest -> IO (Maybe [GraphiteResponse])
metricsJSON req = do
  r <- metricsGet req
  let body = r ^. responseBody
  return $ decode body

-- The raw http request to get the data from Graphite
metricsGet :: GraphiteRequest -> IO (Response ByteString)
metricsGet req = do
  let opt = dataParams req
  let url = reqGraphiteUrl req
  getWith opt url

-- Convert a GraphiteRequest into what's needed for Wreq
dataParams :: GraphiteRequest -> Options
dataParams req = defaults
  & param "from"          .~ [pack . show $ reqFrom req]
  & param "until"         .~ [pack . show $ reqUntil req]
  & param "target"        .~ [reqTarget req]
  & param "maxDataPoints" .~ [reqMaxDataPoints req]
  & param "format"        .~ ["json"]

-- A reasonable set of defaults for Comverge's use
defaultGraphiteRequest :: GraphiteRequest
defaultGraphiteRequest =
  GraphiteRequest {
    reqGraphiteUrl   = "http://graphite.advancedapps.comverge.com/render"
  , reqFrom          = GTime.Relative GTime.Hour (-1)
  , reqUntil         = GTime.Now
  , reqTarget        = "NCVMLAAMON_comverge_com.memory.memory-free"
  , reqMaxDataPoints = "1276"
  }

--------------------------------------------------
-- Data
--------------------------------------------------
data GraphiteRequest = GraphiteRequest {
    reqGraphiteUrl   :: String
  , reqFrom          :: GTime.TimeSpecification
  , reqUntil         :: GTime.TimeSpecification
  , reqTarget        :: Text -- TODO: Represent as a better type?
  , reqMaxDataPoints :: Text -- TODO: Represent as an integer probably...
  } deriving (Show)

data GraphiteResponse = GraphiteResponse {
    target :: String
  , dataPoints :: [DataPoint]
  } deriving (Show)

data DataPoint = DataPoint Integer Integer deriving (Show)

--------------------------------------------------
-- JSON Instances
--------------------------------------------------
instance FromJSON DataPoint where
  parseJSON jsn = do
    [x,y] <- parseJSON jsn
    return $ DataPoint x y

instance FromJSON GraphiteResponse where
  parseJSON (Object o) = do
    t      <- o .: "target"
    p      <- o .: "datapoints" >>= mapM parseJSON
    return $ GraphiteResponse t p
  parseJSON _  = mzero

