module Network.Graphite.TimeSpecification where

import BasePrelude

data TimeUnit =
    Second
  | Minute
  | Hour
  | Day
  | Weeks
  | Months
  | Years

data TimeSpecification =
    Relative TimeUnit Integer
  | Absolute String -- TODO: Smarter format for this
  | Now

instance Show TimeSpecification where
  show (Relative unit amount) = show amount ++ show unit
  show (Absolute t) = t
  show Now = "now"

instance Show TimeUnit where
  show Second = "sec"
  show Minute = "min"
  show Hour   = "h"
  show Day    = "d"
  show Weeks  = "w"
  show Months = "mon"
  show Years  = "y"
