module Watcher.Monitor where

import BasePrelude

data Configuration = Configuration String -- TODO: Make this a real config
  deriving (Show)

data Result = Result String deriving (Show)
forkMonitor :: Monitor -> IO ThreadId
forkMonitor mon = forkIO (runMonitor mon mon)

mkMonitor :: (Monitor -> IO ()) -> Configuration -> IO Monitor
mkMonitor f c = do
  resultVar <- atomically $ newTVar (Result "")
  return Monitor {
    results = resultVar
  , runMonitor = f
  , configuration = c
}


data Monitor = Monitor {
    results       :: TVar Result
  , runMonitor    :: Monitor -> IO ()
  , configuration :: Configuration
  }


writeResult :: Monitor -> Result -> IO ()
writeResult m r = atomically $ writeTVar (results m) r
