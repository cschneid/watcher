module Watcher.Monitor.GraphiteReading where

import BasePrelude

import Watcher.Monitor

graphiteReadingMonitor :: Configuration -> IO Monitor
graphiteReadingMonitor = mkMonitor runGraphiteReadingMonitor

runGraphiteReadingMonitor :: Monitor -> IO ()
runGraphiteReadingMonitor m = updateNumberAndSleep m 0

updateNumberAndSleep m n = do
  putStrLn $ "Entering w/ # " ++ show n

  putStrLn "Before:"
  let tvar1 = results m
  res1  <- atomically $ readTVar tvar1
  print res1

  let newResult = Result (show n)
  putStrLn "About to write new Result: "
  print newResult
  writeResult m newResult

  putStrLn "After:"
  res2  <- atomically $ readTVar tvar1
  print res2

  putStrLn "====================="
  threadDelay 1000000
  updateNumberAndSleep m (n+1)
