module Watcher.WebServer where

import BasePrelude

import Watcher.Types
import Watcher.Monitor

import Control.Monad.IO.Class (liftIO)
import Data.Text.Lazy (pack)
import Web.Scotty

startWebServer :: [Monitor] -> IO ()
startWebServer mons = scotty 3001 (scottyHandlers mons)

startWebServerThread :: [Monitor] -> IO ThreadId
startWebServerThread mons = forkIO (void $ startWebServer mons)

scottyHandlers :: [Monitor] -> ScottyM ()
scottyHandlers mons =
  get "/" $ do
    res <- liftIO $ mapM extractResult mons
    html $ pack $ show res

extractResult :: Monitor -> IO Result
extractResult mon = atomically $ readTVar (results mon)

